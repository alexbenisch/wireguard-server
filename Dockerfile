FROM alpine:latest

#source https://www.cyberciti.biz/faq/how-to-set-up-wireguard-vpn-server-on-alpine-linux/

#Step 1 – Update your system
RUN apk update
RUN apk upgrade

#Step 2 – Installing a WireGuard VPN server on Alpine Linux LTS
RUN apk add wireguard-tools bash vim

#Step 3 – Configuring WireGuard server
RUN mkdir /etc/wireguard/config
WORKDIR /etc/wireguard/config/
COPY script.sh /etc/wireguard/config/script.sh
RUN chmod 755 /etc/wireguard/config/script.sh
COPY wg0.conf /etc/wireguard/wg0.conf
RUN ./script.sh 

# Use iptables masquerade NAT rule
ENV IPTABLES_MASQ=1

# Watch for changes to interface conf files (default off)
ENV WATCH_CHANGES=0

# Modify wg-quick so it doesn't die without --privileged
# Set net.ipv4.conf.all.src_valid_mark=1 on container creation using --sysctl if required instead
RUN sed -i 's/cmd sysctl.*/set +e \&\& sysctl -q net.ipv4.conf.all.src_valid_mark=1 \&\& set -e/' /usr/bin/wg-quick


EXPOSE 51820/udp

# Normal behavior is just to run wireguard with existing configs
#CMD ["/startup"]
